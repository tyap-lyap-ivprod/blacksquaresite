-- Your SQL goes here
CREATE TABLE events(
    id           SERIAL PRIMARY KEY,
    title        TEXT NOT NULL,
    notes        TEXT NOT NULL DEFAULT ' ',
    timestamp_a  TIMESTAMP NOT NULL DEFAULT now(),
    type_pine    INTEGER NOT NULL DEFAULT 1,
    type_poster  INTEGER NOT NULL DEFAULT 1,
    active       BOOLEAN NOT NULL DEFAULT TRUE
);
