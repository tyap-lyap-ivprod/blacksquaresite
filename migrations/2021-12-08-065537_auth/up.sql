-- Your SQL goes here
CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    login TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    salt TEXT NOT NULL,
    admin   BOOLEAN NOT NULL DEFAULT FALSE,
    inta    INTEGER NOT NULL DEFAULT 1,
    active  BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE session(
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    ses_id TEXT NOT NULL,
    active BOOL NOT NULL DEFAULT true,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE groups(
    id SERIAL PRIMARY KEY,
    group_name TEXT NOT NULL,
    description TEXT NOT NULL DEFAULT ''
);

CREATE TABLE usergroup(
    id SERIAL PRIMARY KEY,
    group_id    INTEGER NOT NULL,
    user_id     INTEGER NOT NULL,
    active      BOOL NOT NULL DEFAULT TRUE,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (group_id) REFERENCES groups(id)
);

INSERT INTO groups(group_name, description) VALUES ('admin', 'Can adding users');
