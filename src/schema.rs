table! {
    events (id) {
        id -> Int4,
        title -> Text,
        notes -> Text,
        timestamp_a -> Timestamp,
        type_pine -> Int4,
        type_poster -> Int4,
        active -> Bool,
    }
}

table! {
    groups (id) {
        id -> Int4,
        group_name -> Text,
        description -> Text,
    }
}

table! {
    session (id) {
        id -> Int4,
        user_id -> Int4,
        ses_id -> Text,
        active -> Bool,
    }
}

table! {
    usergroup (id) {
        id -> Int4,
        group_id -> Int4,
        user_id -> Int4,
        active -> Bool,
    }
}

table! {
    users (id) {
        id -> Int4,
        login -> Text,
        password -> Text,
        salt -> Text,
        admin -> Bool,
        inta -> Int4,
        active -> Bool,
    }
}

joinable!(session -> users (user_id));
joinable!(usergroup -> groups (group_id));
joinable!(usergroup -> users (user_id));

allow_tables_to_appear_in_same_query!(
    events,
    groups,
    session,
    usergroup,
    users,
);
