use actix_web::{middleware, App, HttpServer};
use tera::Tera;
use actix_files as fs_ac;
use rand::Rng; // 0.8

use std::env;

use blcksqrsite::get::*;
use blcksqrsite::post::*;
use blcksqrsite::auth;

use actix_session::CookieSession;
use blcksqrsite::config::CONFIG;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    let args: Vec<String> = env::args().collect();
    if args.len() > 1{
        if args[1] == "init"{
            auth::init();
        }
    }
    HttpServer::new(|| {
 //     let tera = Tera::new(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*")).unwrap();
        let tera = Tera::new(&CONFIG.tpl_dir.as_str()).unwrap();
        let _private_key = rand::thread_rng().gen::<[u8; 32]>();
        App::new()
            .data(tera)
            .wrap(CookieSession::signed(&[0; 32]).secure(false))
            .wrap(middleware::Logger::default()) //
            .service(root_page)
            .service(main_get)
            .service(about_get)
            .service(afisha_get)
            .service(instruction_get)

            .service(add_event_get)
            .service(add_event_post)

            .service(auth::login_get)
            .service(auth::login_post)
            .service(auth::logout_post)
            .wrap(middleware::Logger::default()) // enable logger
            //.wrap(CookieSession::signed(&[0; 32]).secure(false))
//get_req
            .service(fs_ac::Files::new("/static", CONFIG.static_dir.as_str()).show_files_listing())
    })
    .bind(CONFIG.bind.clone())?
    
    .run()
    .await
}
