use actix_session::Session;
use actix_web::{HttpResponse,HttpRequest};
use tera::Context;

pub(crate) fn redirect_back(request: HttpRequest)-> HttpResponse {
    let a = request.headers().get("REFERER").unwrap().to_str().unwrap();
    println!("a = {}", a);
    HttpResponse::Found()
            .header("Location", a).finish()
}

pub(crate) fn redirect(a: String)-> HttpResponse {
    HttpResponse::Found()
            .header("Location", a).finish()
}

pub fn str_to_bool(string_val: String) -> bool{
    match string_val.as_str() {
        "true" => true,
        _ => false
    }
}
