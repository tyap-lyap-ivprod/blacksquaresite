use rand::{distributions::Alphanumeric, Rng}; // 0.8
use argon2::{self, Config};
use actix_session::Session as Ses1;
use serde_derive::Deserialize;

use actix_web::{web, Result, get, post};
use actix_web::{error, Error, HttpResponse, HttpRequest};
use crate::config::CONFIG;

use crate::establish_connection;
use crate::models::*;
use crate::utils::{redirect, redirect_back};
use crate::schema::{session, users};
use crate::schema::users::dsl as UserDsl;       //I dont know how deal with duplicates in model names, so i implemented it like this 
use crate::schema::session::dsl as SessionDsl;  //Я не знаю как бороться с дубликатами в именах моделей, поэтому я реализовал это вот так.
use crate::diesel::prelude::*;

#[derive(Deserialize)]
pub struct LoginDate{
    pub username: String,
    pub password: String,
}

// I don`t know why i decided to implement this through models, but i did it
// Не понимаю зачем я реализовал это через модели, но я это сделал.
impl LoginDate {
    pub fn check_pass(self) -> bool{  //checked password
        let config = Config::default();
        let connection = establish_connection();
        let result = &UserDsl::users.filter(UserDsl::login.eq(&self.username))
            .limit(1)
            .load::<Users>(&connection)
            .expect("Error loading users")[0];

        let base_logindate = LoginDate {
            username: result.login.clone(),
            password: result.password.clone()
        };
        //println!("USERNAME1 = {}\nPASSWORD = {}", &self.username, &self.password);
        //println!("USERNAME2 = {}\nPASSWORD = ????", &base_logindate.username );

        let result1 =  if base_logindate.username == self.username {
            let hash1 = argon2::hash_encoded(self.password.as_bytes(), 
                                            format!("{}{}", result.salt, CONFIG.paper).as_bytes(), 
                                            &config).unwrap();
            //println!("\n>>>now_salt = {}", result.salt);
            //println!("\n>>>new_cash={}\n>>>old_cash={}", hash1, base_logindate.password);
            hash1 == base_logindate.password
        }
        else {
            false
        };
        result1
    }
    pub fn create_user(self) {
        let connection = establish_connection();
        let config = Config::default();
        let new_salt = gen_salt();
        let hash = argon2::hash_encoded(self.password.as_bytes(), 
                                        format!("{}{}", new_salt, CONFIG.paper).as_bytes(), 
                                        &config).unwrap();
        //println!("{:?}", hash);
        let addusermodel = AddUser{
            login: self.username,
            salt: new_salt,
            password: hash
        };
        diesel::insert_into(users::table)
            .values(&addusermodel)
            .returning(UserDsl::id)
            .get_result::<i32>(&connection)
            .expect("Error add new user");
    }
}
pub fn gen_salt() -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(32)
        .map(char::from)
        .collect() 
}

pub fn init() {
    //println!("INIT AUTH");
    LoginDate{
        username: CONFIG.username.clone(),
        password: CONFIG.password.clone()
    }.create_user()
}

pub fn gen_session(inp_username: String) -> String {
    let connection = establish_connection();
//    let config = Config::default(); 
    let session_id = gen_salt();
    //print!("Try to get session....");
    //print!("username = {}", &inp_username);
    match &UserDsl::users.filter(UserDsl::login.eq(&inp_username))
                                .limit(1)
                                .load::<Users>(&connection){
        Ok(data) => {
            //println!("...not anon");
            let session_data = AddSession{
                user_id: data[0].id,
                ses_id: session_id,
                active: true
            };
            diesel::insert_into(session::table)
                .values(&session_data)
                .returning(session::dsl::ses_id)
                .get_result::<String>(&connection)
                .expect("Error add mew session")
        },
        _ => {
            //println!("...Anon");
            "anonim".to_string()
        }
    }
}
pub fn check_session(session_inp: &Ses1) -> bool {
    let connection = establish_connection();
    let session_string = session_inp.get::<String>("session").unwrap().unwrap_or("None".to_string());
    match SessionDsl::session.filter(SessionDsl::ses_id.eq(&session_string))
            .limit(1)
            .load::<Session>(&connection){
        Ok(data) =>  match data.len() {
            1 => {
                //println!("session == {}", data[0].ses_id);
                data[0].active
            },
            _ => {
                //println!("data len = {}", data.len());
                //println!("session_string = {}", session_string);
                //println!(">>>>>>>>>>>>>>>>>>>>>>> BLYA");
                false
            }
        },
        Err(_) => false
    }
}
/*
pub fn update_password(username: &String, password: &String){
    let mut con = w_sql::connect_db();
    let config = Config::default();
    let salt = gen_salt();
    let hash = argon2::hash_encoded(password.as_bytes(), 
        format!("{}{}", salt, CONFIG.paper).as_bytes(), 
        &config).unwrap();
                w_sql::password_update(&mut con, &username, &hash, &salt);
}
pub struct UserInfo{
    pub user_id: i32,
    pub username: String,
    pub admin:  bool
}
impl UserInfo {
    pub fn emp() -> UserInfo{
        UserInfo{
            user_id: 0,
            username: "".to_string(),
            admin: false
        }
    }
}
pub fn get_username(
    session: &Session,
    mut client: &mut Client) -> Result<UserInfo, String> {
    let session_id = match &session.get::<String>("session").unwrap() {
        Some(data) => format!("{}",data).to_string(),
        _ => "".to_string()
    };
    let raw_l = w_sql::get_userinfo_from_session(&mut client, &session_id);
    match raw_l.len() {
        1 => Ok(UserInfo{
            user_id:  raw_l[0].get::<_, i32>(0),
            username: raw_l[0].get::<_, String>(1),
            admin:    raw_l[0].get::<_, bool>(2)}
        ),
        _ => Err("Error".to_string())
    }
}
pub fn is_admin(
    con: &mut Client, 
    session: &Session) -> bool{
        let session_id = session.get::<String>("session").unwrap();
        match session_id {
            Some(session_cooked) => {
                let number = w_sql::user_id_from_session(con, &session_cooked);
                match number {
                    Ok(num) => match num {
                        0 => false,
                        _ => match w_sql::is_admin(con, &num) {
                            Ok(data) => data.len() > 0,
                            Err(_) => false
                        }
                    },
                    Err(_) => false
                }
            },
            _ => false
        }
}
pub fn ses_in_group(
    mut client: &mut Client,
    session: &Session,
    group: &i32) -> bool {
        match get_username(&session, &mut client) {
            Ok(user) => {
                println!("\n\n>>>{}", &user.user_id);
                w_sql::is_in_group(&mut client, &user.user_id, &group)
            },
            Err(_) => false
        }
}
*/

//GET request

#[get("/login")]
async fn login_get(
    tmpl: web::Data<tera::Tera>,
    session: Ses1
) -> Result<HttpResponse, Error> {
    let mut ctx = tera::Context::new();
    let session_res = &check_session(&session);
    ctx.insert("session", session_res);
    //println!("login_get {}", session_res);
    let s = tmpl.render("login.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError(":("))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}


//POST request

#[post("/login")] //Login
async fn login_post(
    form: web::Form<UserForm>,
    session: Ses1,
    request: HttpRequest
) -> Result<HttpResponse, Error> {

        let logindate = LoginDate {
            username: form.username.clone(),
            password: form.password.clone()
        }.check_pass();
        Ok(match logindate{
            true => {
                let ses: String = gen_session(form.username.clone());
                //println!("\n\nacab>>> {}", &ses);
                session.set("session", ses).unwrap();
                redirect("/".to_string())
            },
            false => {
                redirect_back(request)
            }
        })
}
#[post("/logout")] //Logout
async fn logout_post(session: Ses1) -> Result<HttpResponse, Error> {
    session.clear();
    Ok(redirect("/login".to_string()))
}

