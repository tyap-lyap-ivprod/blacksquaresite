use actix_web::{web, Result, get};
use crate::utils::*;
use actix_web::{error, Error, HttpResponse};
use crate::schema::events::dsl::*;
use actix_session::Session;

use crate::establish_connection;
use crate::models::*;
use crate::diesel::prelude::*;
use crate::auth::check_session;

#[get("/main")]
async fn main_get(tmpl: web::Data<tera::Tera>) -> Result<HttpResponse, Error> {
    let ctx = tera::Context::new();
    println!("main_get");
    let s = tmpl.render("main.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError(":("))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/add_event")]
async fn add_event_get(tmpl: web::Data<tera::Tera>,
    session: Session) -> Result<HttpResponse, Error> {
    Ok(match check_session(&session){
        true => {
            let ctx = tera::Context::new();
            println!("add_event_get");
            let s = tmpl.render("add_event.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError(":("))?;

            HttpResponse::Ok().content_type("text/html").body(s)
        },
        false => {
            redirect("/login".to_string())
        }
    })
}

#[get("/instruction")]
async fn instruction_get(tmpl: web::Data<tera::Tera>) -> Result<HttpResponse, Error> {
    let ctx = tera::Context::new();
    println!("instruction_get");
    let s = tmpl.render("instruction.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError(":("))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}


#[get("/about")]
async fn about_get(tmpl: web::Data<tera::Tera>) -> Result<HttpResponse, Error> {
    let ctx = tera::Context::new();
    println!("about_get");
    let s = tmpl.render("about.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError(":("))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/afisha")]
async fn afisha_get(
    tmpl: web::Data<tera::Tera>
) -> Result<HttpResponse, Error>{

    let connection = establish_connection();
    let results = events.filter(active.eq(true))
        .limit(5)
        .load::<Events>(&connection)
        .expect("Error loading posts");
    let mut ctx = tera::Context::new();
    ctx.insert("events", &results);
    let s = tmpl.render("afisha.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError(":("))?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/")]
async fn root_page(
) -> Result<HttpResponse, Error>{
    Ok(redirect("main".to_string()))
}

