use actix_web::{web, Result, post};
use crate::models::*;
use crate::auth::check_session;
use actix_web::{Error, HttpResponse, HttpRequest};
use actix_session::Session;

use crate::schema::events;
use crate::establish_connection;

use crate::schema::events::dsl::*;
use crate::diesel::prelude::*;

use crate::utils::*;


#[post("/add_event")]
async fn add_event_post(form: web::Form<AddEvents>,
                        session: Session,
                        request: HttpRequest) -> Result<HttpResponse, Error> {
    Ok(match check_session(&session){
        true => {
            let connection = establish_connection();
            let event = AddEvents{
                title: form.title.clone(),
                notes: form.notes.clone(),
                type_pine: form.type_pine.clone(),
                type_poster: form.type_poster.clone(),
                active: form.active.clone(), 
            };
            diesel::insert_into(events::table)
                .values(&event)
                .returning(id)
                .get_result::<i32>(&connection)
                .expect("Error saving new post");
            redirect_back(request)
        },
        false => redirect("/error/zhulick".to_string())
    })
}

