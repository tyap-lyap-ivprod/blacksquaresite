use super::schema::{events, users, session};
use serde_derive::{Serialize, Deserialize};
use chrono::NaiveDateTime;

#[derive(Queryable, Serialize)]
pub struct Events{
    pub id: i32,
    pub title: String,
    pub notes: String,
    pub timestamp_a: NaiveDateTime,
    pub type_pine: i32,
    pub type_poster: i32,
    pub active: bool, 
}
#[derive(Insertable, Deserialize)]
#[table_name="events"]
pub struct AddEvents{
    pub title: String,
    pub notes: String,
    pub type_pine: i32,
    pub type_poster: i32,
    pub active: bool, 
}

#[derive(Insertable)]
#[table_name="events"]
pub struct EventId{
    pub id: i32
}

#[derive(Queryable)]
pub struct Users{
    pub id: i32,
    pub login: String,
    pub password: String,
    pub salt: String,
    pub admin: bool,
    pub inta: i32,
    pub active: bool
}

#[derive(Deserialize)]
pub struct UserForm{
    pub username: String,
    pub password: String
}

#[derive(Insertable)]
#[table_name="users"]
pub struct AddUser{
   pub login: String,
   pub password: String,
   pub salt: String
}

#[derive(Queryable)]
pub struct UserGroup {
    pub id: i32,
    pub user_id: i32,
    pub group_id: i32,
    pub active: bool
}

#[derive(Queryable)]
pub struct Groups{
    pub id: i32,
    pub group_name: String,
    pub description: String
}

#[derive(Queryable)]
pub struct Session{
    pub id: i32,
    pub user_id: i32,
    pub ses_id: String,
    pub active: bool
}

#[derive(Insertable)]
#[table_name="session"]
pub struct AddSession{
    pub user_id: i32,
    pub ses_id: String,
    pub active: bool
}
